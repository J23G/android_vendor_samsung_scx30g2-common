# Copyright (C) 2012 The CyanogenMod Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

PROPRIETARIES := \
    bin/at_distributor \
    bin/npsmobex \
    bin/refnotify \
    bin/engpc \
    lib/libatparser.so \
    bin/gpsd \
    bin/gps.cer \
    etc/gps.conf \
    lib/hw/gps.default.so \
    lib/liblcsagent.so \
    lib/libboost.so \
    lib/egl/libGLES_mali.so \
    lib/libfactoryutil.so \
    lib/libomission_avoidance.so \
    lib/libatchannel.so \
    etc/wifi/bcmdhd_apsta.bin \
    etc/wifi/bcmdhd_mfg.bin \
    etc/wifi/bcmdhd_sta.bin \
    etc/wifi/nvram_mfg.txt \
    vendor/firmware/BCM43430A1_V0034.0089.hcd \
    vendor/firmware/vbc_eq \
    bin/rild \
    bin/modemd \
    bin/nvitemd \
    bin/phoneserver \
    bin/modem_control \
    bin/ext_data.sh \
    bin/ext_kill.sh \
    bin/ext_symlink.sh \
    bin/prepare_param.sh \
    lib/libril.so \
    lib/libreference-ril_sp.so \
    lib/libsecril-client.so \
    lib/libsecnativefeature.so \
    lib/libomx_aacdec_sprd.so \
    lib/libomx_avcdec_hw_sprd.so \
    lib/libomx_avcenc_hw_sprd.so \
    lib/libomx_vpxdec_hw_sprd.so \
    lib/libomx_m4vh263dec_hw_sprd.so \
    lib/libomx_m4vh263enc_hw_sprd.so \
    lib/libomx_mp3dec_sprd.so \
    bin/sswap \
    lib/lib_SoundAlive_play_ver125e.so \
    lib/soundfx/libaudiosa_sec.so \
    lib/soundfx/libaudioeffectoffload.so \
    etc/audio_effects.conf \
	etc/audio_hw.xml \
	etc/audio_para \
	etc/audio_policy.conf \
	etc/codec_pga.xml \
	etc/gps.xml \
	etc/media_codecs.xml \
	etc/media_codecs_performance.xml \
	etc/media_profiles.xml \
	etc/tiny_hw.xml \
	lib/hw/camera.sc8830.so \
	lib/hw/sensors.sc8830.so \
	lib/libface_finder.so \
	lib/libisp.so \
	usr/keylayout/Generic.kl \
	lib/hw/sprd_gsp.sc8830.so \
	lib/hw/hwcomposer.sc8830.so \
	lib/hw/gralloc.default.so \
	lib/hw/fm.sc8830.so \
	lib/hw/audio_policy.default.so \
	lib/hw/audio.usb.default.so \
	lib/hw/audio.r_submix.default.so \
	lib/hw/audio.a2dp.default.so \
	lib/hw/audio.primary.default.so \
	lib/hw/audio.primary.sc8830.so \
	etc/init.d/30zram \
	etc/init.d/90userinit

PRODUCT_COPY_FILES += \
    $(foreach f,$(PROPRIETARIES),vendor/samsung/scx30g2-common/proprietary/$(f):system/$(f))
